const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const Joi = require('joi');
const path = require('path');
var methodOverride = require('method-override');
var bodyParser = require('body-parser');
const Post = require('./models/post.js');

const PORT = process.env.PORT || 4444;
const app = express();
let films = [];

app.set('view engine', 'ejs');

app.listen(PORT, () => {
    console.log(`the server is running on port ${PORT}...`);
    mongoose
        .connect(
            'mongodb+srv://morozov2717:QHvL9dCVpPEvJJGE@cluster0.yktpj5u.mongodb.net/AllMovies?retryWrites=true&w=majority',
            {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            }
        )
        .then((res) => {
            console.log('Connected to DB');
        })
        .catch((error) => console.log(error));
});

app.use(methodOverride('_method'));
app.use(
    morgan(':method :url :status :res[content-length] - :response-time ms')
);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.render(path.resolve(__dirname, 'views', 'home.ejs'));
});

app.get('/films/add', (req, res) => {
    res.render(path.resolve(__dirname, 'views', 'add.ejs'));
});

app.get('/films', async (req, res) => {
    await Post.find()
        .then((result) => {
            films = result;
        })
        .catch((error) => console.log(error));
    res.render(path.resolve(__dirname, 'views', 'films.ejs'), { films });
});

app.get('/genre', async (req, res) => {
    await Post.find()
        .then((result) => {
            films = result;
        })
        .catch((error) => console.log(error));
    res.render(path.resolve(__dirname, 'views', 'genre.ejs'), { films });
});

app.get('/genre/:id', async (req, res) => {
    await Post.find()
        .then((result) => {
            films = result;
        })
        .catch((error) => console.log(error));
    let genre = [];
    films.forEach((element) => {
        if (element.genre == req.params.id) genre.push(element);
    });
    res.render(path.resolve(__dirname, 'views', 'allgenre.ejs'), {
        genre,
    });
});

app.get('/edit/:id', async (req, res) => {
    await Post.findById(req.params.id)
        .then((post) =>
            res.render(path.resolve(__dirname, 'views', 'change.ejs'), {
                post,
            })
        )
        .catch((error) => console.log(error));
});

app.put('/edit/:id', (req, res) => {
    const schema = Joi.object({
        name: Joi.string().min(3).required(),
        genre: Joi.string().min(3).required(),
        description: Joi.string().required(),
    });
    const { error } = schema.validate(req.body);
    if (error) {
        return res.status(400).send(error.datails[0].message);
    }

    const { genre, name, description } = req.body;
    Post.findByIdAndUpdate(req.params.id, { genre, name, description })
        .then((result) => {
            res.redirect('/films');
        })
        .catch((error) => console.log(error));
});

app.delete('/films/:id', (req, res) => {
    Post.findByIdAndDelete(req.params.id)
        .then((result) => {
            res.status(200);
            res.redirect('/films');
        })
        .catch((error) => console.log(error));
});

app.post('/films', (req, res) => {
    const schema = Joi.object({
        name: Joi.string().min(3).required(),
        genre: Joi.string().min(3).required(),
        description: Joi.string().required(),
    });
    const { error } = schema.validate(req.body);
    if (error) {
        return res.status(400).send(error.datails[0].message);
    }
    const { genre, name, description } = req.body;
    const post = new Post({ genre, name, description });
    post.save()
        .then((result) => {
            console.log('Data has been successfully recorded');
            res.redirect('/films');
        })
        .catch((error) => {
            console.log(error);
        });
});

app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});
